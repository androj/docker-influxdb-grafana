# docker + influxdb + grafana ( + telegraf )

Docker-compose files for a simple and up to date monitoring stack.

Forked from https://github.com/nicolargo/docker-influxdb-grafana.git

In the upstream repo, a telegraf container is also spun up, ostensibly to monitor the container host itself.
Here however, we scrap that in exchange for installing telegraf directly on that host, that way it reports collected metrics more transparently.

## Getting started

```
git clone git@gitlab.com:androj/docker-influxdb-grafana.git
cd docker-influxdb-grafana
docker pull grafana/grafana
docker pull influxdb
```

Run your stack:

```
./run
```

Show me the logs:

```
docker-compose logs
```

Stop it:

```
docker-compose stop
docker-compose rm
```

Update it:

```
git pull
docker pull grafana/grafana
docker pull influxdb
```

If you want to run Telegraf, edit the telegraf.conf to your needs and add it to each container in docker-compose.yml
